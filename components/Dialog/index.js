// components/Dialog/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    visible: Boolean,
    title: String,
    buttontype: { // authorize | phone
      type: String,
      value: 'normal'
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    closeDialog () {
      this.triggerEvent('close', {}, {})
    },
    confirm (e) {
      this.triggerEvent('confirm', e, {})
    },
    stopEvent () {
      return
    }
  }
})
