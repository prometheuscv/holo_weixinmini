import { model } from '../../utils/util'
const app = getApp()

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    modelId: {
      type: String,
      value: ''
    }
  },

  methods: {
    backToModel () {
      wx.reLaunch({
        url: '/pages/index/index?q=' + this.data.modelId
      })
    }
  },
  ready () {
    this.setData({
      statusBarHeight: app.globalData.statusBarHeight,
      titleBarHeight: app.globalData.titleBarHeight
    })
  }
})
