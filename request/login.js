function token () {}

token.get = function () {
  var token_ = wx.getStorageSync('tokenData')

  if (checkExpire(token_.expire)){
    return token_
  }
  else{
    token.clear()
    return null;
  }
}
token.set = function (val) {
  return wx.setStorageSync('tokenData', val)
}
token.clear = function (val) {
  wx.removeStorageSync('tokenData');
}

function checkExpire(expire){
  var expire_ = expire/1000;
  var timestamp = Date.parse(new Date());
  timestamp = timestamp / 1000;

  if (expire_ > timestamp){
      return true;
  }
  else{
    return false;
  }
}

// 获取用户信息
function wxGetUserInfo () {
  return new Promise(function (resolve, reject) {
    wx.getUserInfo({
      success: function(resUserInfo) {
        wx.login({
          success (res) {
            if (res.code) {
              let params = {
                js_code: res.code,
                raw_data: resUserInfo.rawData,
                signature: resUserInfo.signature,
                iv: resUserInfo.iv,
                encrypted_data: resUserInfo.encryptedData
              }
              authorize(params).then(data => {
                if (data && data.code === 0) {
                  resolve(data);
                  // 设置 token
                  token.set(data.data.access_token)
                } else {
                  reject({
                    errMsg: '请求异常'
                  })
                }
              }).catch(err => {
                reject({ errMsg: '请求失败' })
              })
            } else {
              reject({ errMsg: res.errMsg })
            }
          },
          fail (res) {
            reject({ errMsg: res.errMsg })
          }
        })
      },
      fail (res) {
        reject({ errMsg: res.errMsg })
      }
    })
  })
}

export {
  wxGetUserInfo,
  token
}
