import { h5View } from '../../constant'
import { token } from '../../request/login'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    originUrl: "",
    webUrl: "",
    modelID: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)

    if (options.type === "2") {
      this.setData({
        modelID: options.modelID,
        webUrl: h5View + "?bootType=" + options.type + "&modelID=" + options.modelID
      })
      //console.log(this.data.webUrl)
    }
    else if (options.type === "1") {
      wx.hideShareMenu()
      this.setData({
        modelID: options.modelID,
        webUrl: h5View + "?bootType=" + options.type + "&modelID=" + options.modelID + "&userToken=" + token.get().token
      })
    }
    else if (options.type === "3") {
      this.setData({
        modelID: options.modelID,
        webUrl: h5View
      })
      //console.log(this.data.webUrl)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(e) {
    let imageurl = "";
    imageurl = wx.getStorageSync('shareImage')

    return {
      title: '追光魔方小程序',
      path: '/pages/modelView/index?type=2&modelID=' + this.data.modelID,        // 默认是当前页面，必须是以‘/’开头的完整路径
      imageUrl: imageurl    //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持PNG及JPG，不传入 imageUrl 则使用默认
    }
  }
})