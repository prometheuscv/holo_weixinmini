import { wxGetUserInfo, token } from '../../request/login'
import { model, getCompleteModelUrl } from '../../utils/util'
import { host,request_rount } from '../../constant'

const MAX_LEN = 20

Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    wxUserInfo: null,
    userInfo: null,

    page: 0,
    dataList: [],
    qrModelID: "",

    TYPE_1: 'authorize',
    TYPE_2: 'phone',
    isLogin: false,
    showDialog: false,
    dialogTitle: '',
    dialogType: 'normal',
    dialogContent: ''
  },
  onLoad(option) {
    //console.log("onLoad")
    if (this.data.qrModelID.length !== 0) {
      this.setData({
        qrModelID: ""
      })
    }

    var scene = decodeURIComponent(option.q)
    if (scene.length !== 0) {
      var parameterStr = scene.split("?")
      let id = parameterStr[1]

      if (id !== (null || undefined)) {
        this.setData({
          qrModelID: id
        })
      }
    }
  },
  onReady(option) {
    //console.log("onReady")
    this.setData({

    })
    if (token.get()) {
      this.getUserInfo().then(userInfodata => {
        if (userInfodata.statusCode === 403) {
          this.onNetworkError()
        }
        else {
          this.setData({ userInfo: userInfodata.data })
          this.getUserModelList().then(userModelListdata => {
            this.setData({ dataList: userModelListdata.data.data, isLogin: true })

            if (this.data.qrModelID !== (undefined || null)) {
              if (this.data.qrModelID.length !== 0) {
                this.onJumpPageFromQR()
              }
            }

          })
        }
      }, fail => { console.log("失败") })
    } else {
      if (this.data.qrModelID !== (undefined || null)) {
        if (this.data.qrModelID.length !== 0) {
          this.setDialog(this.data.TYPE_1)
        }
      }
    }
  },
  onShow() {
    if (this.data.isLogin) {
      this.getUserModelList().then(userModelListdata => {
        this.setData({ dataList: userModelListdata.data.data, isLogin: true })
      })
    }
  },
  onPullDownRefresh() {
    if (token.get()) {
      this.getUserModelList().then(userModelListdata => {
        this.setData({ dataList: userModelListdata.data.data })
      })
      wx.stopPullDownRefresh()
    } else {
      wx.stopPullDownRefresh()
      this.setData({
        userInfo: null,
        dataList: []
      })
    }
  },

  onJumpPageFromQR() {
    console.log(this.data.qrModelID)
    wx.navigateTo({
      url: '../modelView/index?type=1&modelID=' + this.data.qrModelID
    })
  },

  onNetworkError() {
    this.setData({ wxUserInfo: null, userInfo: null })
    token.clear()
  },

  onShowDialog1() {
    this.setDialog(this.data.TYPE_1)
  },

  closeDialog() {
    this.setDialog()
  },

  getWXUserInfo(e) {
    wx.showLoading({ mask: true })
    wx.getUserInfo({
      success: res => {
        console.log(res)
        this.setData({
          wxUserInfo: res.userInfo
        })
        wx.hideLoading()
        this.setDialog(this.data.TYPE_2)
      }
    })
  },

  doLogin(e) {
    wx.login({
      success: loginRes => {
        wx.request({
          url: host + request_rount.do_login,
          method: 'POST',
          data: {
            code: loginRes.code,
            encrypted_data: e.detail.encryptedData,
            iv: e.detail.iv
          },
          success: res => {
            console.log(this.data.wxUserInfo.nickName)
            token.set(res.data);
            wx.hideLoading();
            this.setDialog()

            this.setUserInfo(this.data.wxUserInfo).then(data => {
              this.getUserInfo().then(userInfodata => {
                this.setData({ userInfo: userInfodata.data })
                this.getUserModelList().then(userModelListdata => {
                  this.setData({ dataList: userModelListdata.data.data })
                  if (this.data.qrModelID !== (undefined || null)) {
                    if (this.data.qrModelID.length !== 0) {
                      this.onJumpPageFromQR()
                    }
                  }
                })
              })
            })
          }
        })
      }
    })
  },

  getUserModelList: function () {
    return new Promise(function (resolve, reject) {
      let token_ = token.get()
      if (token_) {
        wx.request({
          url: host + request_rount.user_model_list,
          method: 'GET',
          header: {
            "pm-token": token_.token
          },
          success: function (res) {
            //console.log(res);
            resolve(res)
          }
        })
      } else {
        wx.showToast({
          title: '获取信息失败',
        })
      }
    })
  },

  setUserInfo(requestData) {
    return new Promise(function (resolve, reject) {
      let token_ = token.get()
      console.log("token_", token_)
      if (token_) {
        wx.request({
          url: host + request_rount.set_user_info,
          method: 'PUT',
          data: {
            name: requestData.nickName,
            avatar: requestData.avatarUrl
          },
          header: {
            "pm-token": token_.token
          },
          success: function (res) {
            console.log(res);
            resolve(res)
          },
          fail: function () {
            wx.showToast({
              title: '设置信息失败',
            })
            reject()
          }
        })
      }
      else {
        wx.showToast({
          title: '设置信息失败',
        })
      }
    })
  },

  getUserInfo() {
    return new Promise(function (resolve, reject) {
      let token_ = token.get()
      //console.log(token_)
      if (token_) {
        wx.request({
          url: host + request_rount.get_user_info,
          method: 'GET',
          header: {
            "pm-token": token_.token
          },
          success: function (res) {
            //console.log(res)
            resolve(res)
          },
          fail: function (res) {
            reject(res)
            wx.showToast({
              title: '获取信息失败',
            })
          }
        })
      }
      else {
        reject(res)
        wx.showToast({
          title: '获取信息失败',
        })
      }
    })
  },

  // 弹窗状态设置
  setDialog(type, _data) {
    let data = { ..._data, dialogType: type }
    switch (type) {
      case this.data.TYPE_1:
        data.showDialog = true
        data.dialogTitle = '授权登录'
        data.dialogContent = '为保证您能正常使用, 此应用需要获取您的昵称、头像等基本信息'
        break
      case this.data.TYPE_2:
        data.showDialog = true
        data.dialogTitle = '绑定手机号码'
        data.dialogContent = '根据《网络安全法》相关规定, 账号需绑定手机实名, 当前网络环境安全, 请绑定您的手机号码'
        break
      default:
        data.showDialog = false
    }
    this.setData(data)
  },

  // 弹窗操作
  dialogConfirm(e) {
    console.log(e)
    switch (this.data.dialogType) {
      case this.data.TYPE_1:
        this.getWXUserInfo()
        break
      case this.data.TYPE_2:
        this.doLogin(e.detail)
        break
    }
  },

  browseModel: function (e) { 
    //console.log(e)
    let query = e.currentTarget.dataset['1'];
    wx.setStorageSync('shareImage', query.image);
    wx.navigateTo({
      url: '../modelView/index?type=2&modelID=' + query.id 
    })
  },

  showDefaultModel:function(){

    wx.navigateTo({
      url: '../modelView/index?type=3&modelID=' + 666
    })
    //console.log("showDefaultModel")
  },

  onShareAppMessage(e) {
    if (e.from === 'button') {
      // 通过按钮触发
      //console.log("按钮分享")
      //console.log(e)
      var data = e.target.dataset
      return {
        title: '追光魔方小程序',
        path: '/pages/modelView/index?type=2&modelID=' + data.id,
        imageUrl: data.image,
      }
    }

    return {
      title: '追光魔方小程序',
    }
  }
})