//const host = 'https://qp5v5r9es1.execute-api.cn-north-1.amazonaws.com.cn/dev'
const host = 'https://xcx.prometh.xyz'
const h5View = "https://holopreview.prometh.xyz"

const request_rount = {
  do_login: '/users/wx_sign_in', // post
  get_user_info: '/users', // get
  set_user_info: '/users', // put
  user_model_list: '/models', // get
}

export {
  host,
  request_rount,
  h5View
}
