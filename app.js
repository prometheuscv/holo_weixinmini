import { model } from './utils/util'

App({
  onLaunch () {
    // 获取系统详情
    wx.getSystemInfo({
      success: (res) => {
        let headerH = wx.getMenuButtonBoundingClientRect()
        this.globalData.statusBarHeight = res.statusBarHeight //状态栏高度
        //this.globalData.titleBarHeight = totalTopHeight - res.statusBarHeight // 标题高度
        this.globalData.titleBarHeight = (headerH.bottom + headerH.top) - (res.statusBarHeight * 2)
      },
      failure: () => {
        this.globalData.statusBarHeight = 20
        this.globalData.titleBarHeight = 44
      }
    })
  },
  onHide () {
    console.log('onHide')
  },
  globalData: {
  }
})